// console.log("WAGMI");


/*Create a simple Shipping Function for an online store that will use functions, if-else and switch statements
Features:
	1. Calculates shipping price via the item's weight. (use if-else)
		less than half a kilo is P90
		more than half a kilo and less than or equal to a kilo is P120
		more than a kilo and less than or equal to 3 kilos is P250
		more than 3 kilos and less than or equal to 5 kilos is P380
		more than 5 kilos and less than or equal to 10 kilos is P550
		return a message that will display more than 10 kilos must not be allowed */

function calculateShippingPrice(weight, destination) {
  let shippingFee;

  // Calculate shipping price based on weight
  if (weight < 0.5) {
    shippingFee = 90;
  } else if (weight <= 1) {
    shippingFee = 120;
  } else if (weight <= 3) {
    shippingFee = 250;
  } else if (weight <= 5) {
    shippingFee = 380;
  } else if (weight <= 10) {
    shippingFee = 550;
  } else {
    return "Shipping not allowed for more than 10 kilos";



  }


/*  2. Can select a shipping method from local or overseas (use switch)
  		multiply by 1.0 the shipping fee when local is selected
  		multiply by 1.5 the shipping fee when overseas is selected*/
  // Multiply shipping fee by multiplier based on destination
  let multiplier;
  switch (destination) {
    case "local":
      multiplier = 1.0;
      break;
    case "overseas":
      multiplier = 1.5;
      break;
    default:
      return "Invalid destination";
  }
  shippingFee *= multiplier;

  // Return message with total shipping fee
  return `Total shipping fee: P${shippingFee.toFixed(2)}`;
}


/*3. Return a message that will inform user for the total amount of shipping fee
	4. Invoke the function in the console.
*/
// Invoke the function with sample parameters and log the result to console
console.log(calculateShippingPrice(0.3, "local")); // Total shipping fee: P90.00
console.log(calculateShippingPrice(0.6, "local")); // Total shipping fee: P90.00
console.log(calculateShippingPrice(2, "local")); // Total shipping fee: P90.00
console.log(calculateShippingPrice(4, "local")); // Total shipping fee: P90.00
console.log(calculateShippingPrice(7, "local")); // Total shipping fee: P90.00
console.log(calculateShippingPrice(12, "local")); // Total shipping fee: P90.00
console.log(calculateShippingPrice(0.3, "overseas")); // Total shipping fee: P90.00
console.log(calculateShippingPrice(0.6, "overseas")); // Total shipping fee: P90.00
console.log(calculateShippingPrice(2, "overseas")); // Total shipping fee: P90.00
console.log(calculateShippingPrice(4, "overseas")); // Total shipping fee: P90.00
console.log(calculateShippingPrice(7, "overseas")); // Total shipping fee: P90.00
console.log(calculateShippingPrice(12, "overseas")); // Total shipping fee: P90.00

